#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/**
 * Sizes of input strings
 */
const int TITLE_SIZE = 60;
const int AUTHOR_SIZE = 50;
const int CUSTOMER_NAME_SIZE = 50;


/**
 * Available ANSI colors for colourful CLI.
 */
enum ANSI_COLOR {
    WHITE   = 30,
    RED     = 31,
    GREEN   = 32,
    YELLOW  = 33,
    BLUE    = 34,
    MAGENTA = 35,
    CYAN    = 36,
};

/**
 * ANSI font can be normal or bold.
 */
enum ANSI_STYLE {
    NORMAL  = 0,
    BOLD    = 1,
};

/**
 * Set ANSI color for upcoming text output.
 * @param color     Color.
 * @param style     Style (normal/bold).
 */
void ansi_color(enum ANSI_COLOR color, enum ANSI_STYLE style) { printf("\033[%d;%dm", style, color); }

/**
 * Reset ANSI color and style to normal.
 */
void ansi_reset() { printf("\033[0m"); }


/**
 * Linked-list book structure 
 */
struct Book {
    char* title;
    char* author;
    int year;
    float book_rate;
    struct Book *next_book;
    struct Book *prev_book;
};


/**
 * Linked-list customer structure
 */
struct customer {
    char *name;
    int age;
    float customer_rate;
    struct customer *next_customer;
    struct customer *prev_customer;
};

// Defining Customer type
typedef struct customer Customer; 


/**
 * Creates Book struct by given parameters.
 * @param title         Title of the book
 * @param author        Author name
 * @param year          Book release date
 * @param book_rate     Book rate (float value 0-1)
 * @result              A pointer to the Book structure
 */
struct Book * new_book(char* title, char* author, int year, float book_rate) {
    struct Book * book = malloc(sizeof(struct Book));
    book->title = title;
    book->author = author;
    book->year = year;
    book->book_rate = book_rate;
    book->next_book = NULL;
    book->prev_book = NULL;
    return book;
}


/**
 * Creates Customer struct by given parameters.
 * @param name              Name of a customer
 * @param age               Age or a customer
 * @param customer_rate     Customer rate
 * @result                  A pointer to the Customer structure
 */
Customer * new_customer(char* name, int age, float customer_rate) {
    Customer * customer = malloc(sizeof(Customer));
    customer->name = name;
    customer->age = age;
    customer->customer_rate = customer_rate;
    customer->next_customer = NULL;
    customer->prev_customer = NULL;
    return customer;
}


// Global pointers to first and last elements of linked list with books
struct Book *first_book = NULL;
struct Book *last_book;
Customer *first_customer = NULL;
Customer *last_customer;


/**
 * Clears file from some rubbish after scanf, printf, gets.
 * @param in    pointer to file to clear
 */
void tidy ( FILE *in ) {
    char ch;
    while ((ch = getc(in)) != EOF && ch != '\n');
}


/**
 * Safely printing text with fflush 
 * @param s    text to print
 */
void print_text(char * s) {
    printf("%s", s);
    fflush(stdout);
}

/**
 * Clears terminal
 */
void clear_terminal() {
    system("@cls||clear");
}


/**
 * Safely printing info string
 * @param s    text to print
 */
void print_info(char * s) {
    ansi_color(BLUE, NORMAL);
    print_text(s);
    ansi_reset();
}

/**
 * Safely printing error string
 * @param s    text to print
 */
void print_error(char * s) {
    ansi_color(RED, BOLD);
    print_text(s);
    ansi_reset();
}

/**
 * Safely printing question string
 * @param s    text to print
 */
void print_question(char * s) {
    ansi_color(WHITE, BOLD);
    print_text(s);
    ansi_reset();
}


/**
 * Safely printing title string
 * @param s    text to print
 */
void print_title(char * s) {
    ansi_color(MAGENTA, BOLD);
    print_text(s);
    ansi_reset();
}


void press_enter_to_continue() {
    print_text("\nPress Enter to Continue...");
    while( getchar() != '\n' );
}

/**
 * Safely printing acception string
 * @param s    text to print
 */
void print_accept(char * s) {
    ansi_color(GREEN, BOLD);
    print_text(s);
    ansi_reset();
}


/**
 * Safely read's string
 * @param size  size of string to read
 * @result      string recieved from stdin
 */
char * read_string(const int size) {
    char * str = malloc(size * sizeof(char));
    fgets(str, size, stdin);
    char * pos;
    if ((pos=strchr(str, '\n')) != NULL) *pos = '\0';
    return str;
}


/**
 * Reads book from terminal
 * @result  Pointer to the new book struct
 */
struct Book * read_book() {
    print_question("Enter a title: ");
    char * title = read_string(TITLE_SIZE);
    
    print_question("Enter an author: ");    
    char * author = read_string(AUTHOR_SIZE);
                
    int year;
    float book_rate;
    
    print_question("Enter a year: ");
    scanf("%d", &year);
    tidy(stdin);
    
    print_question("Enter a book rate: ");
    scanf("%f", &book_rate);
    tidy(stdin);

    return new_book(title, author, year, book_rate);
}


/**
 * Reads customer from terminal
 * @result  Pointer to the new customer struct
 */
Customer * read_customer() {
    print_question("Enter a name: ");
    char * name = read_string(CUSTOMER_NAME_SIZE);
    
    int age;
    float customer_rate;
    
    print_question("Enter customer's age: ");
    scanf("%d", &age);
    tidy(stdin);
    
    print_question("Enter customer's rate: ");
    scanf("%f", &customer_rate);
    tidy(stdin);

    return new_customer(name, age, customer_rate);
}


/**
 * Prints book structure to the terminal
 * @param  book     pointer to the Book structure to print
 */
void print_book(struct Book * book) {
    print_question("-------------------------\n");
    print_info("Title: ");
    print_text(book->title);
    print_info("\nAuthor: ");
    print_text(book->author);
    print_info("\nYear: ");
    printf("%d\n", book->year);
    print_info("Book rate: ");
    printf("%.1f\n", book->book_rate);
    print_question("-------------------------\n");
    fflush(stdout);
}


/**
 * Prints book structure to the terminal
 * @param  customer     pointer to the Book structure to print
 */
void print_customer(Customer * customer) {
    print_text("-------------------------\n");
    print_info("Name: ");
    print_text(customer->name);
    print_info("\nAge: ");
    printf("%d\n", customer->age);
    print_info("Customer rate: ");
    printf("%.1f\n", customer->customer_rate);
    print_text("-------------------------\n");
    fflush(stdout);
}


/**
 * Prints all books from linked list with books
 */
void print_book_list() {
    int counter = 0;
    for (struct Book * b = first_book; b != NULL; b = b->next_book) {
        counter++;
        print_book(b);
    }
    if (counter == 0) {
        print_error("There are no books in the library.\n");
    }
}

/**
 * Prints all books from linked list with books
 */
void print_customer_list() {
    int counter = 0;
    for (Customer * c = first_customer; c != NULL; c = c->next_customer) {
        counter++;
        print_customer(c);
    }
    if (counter == 0) {
        print_error("There are no customers in the library.\n");
    }
}


/**
 * Adds book to the linked list
 */
void add_book(struct Book *book) {
    if (last_book == NULL) {
        first_book = book;
        last_book = book;
        return;
    }
    last_book->next_book = book;
    book->prev_book = last_book;
    last_book = book;
}


/**
 * Ads customer to the linked list
 */
void add_customer(Customer* customer) {
    if (last_customer == NULL) {
        first_customer = customer;
        last_customer = customer;
        return;
    }
    last_customer->next_customer = customer;
    customer->prev_customer = last_customer;
    last_customer = customer;
}


/**
 * Finds book in linked list
 * @param title title of the book
 * @result      pointer to the found book
 */
struct Book * find_book(char * title) {
    for (struct Book * b = first_book; b != NULL; b = b->next_book) {
        if (strcmp(title, b->title) == 0) {
            return b;
        }
    }
    return NULL;
}


/**
 * Finds customer in linked list
 * @param name  name of the customer
 * @result      pointer to the found customer
 */
Customer * find_customer(char * name) {
    for (Customer * c = first_customer; c != NULL; c = c->next_customer) {
        if (strcmp(name, c->name) == 0) {
            return c;
        }
    }
    return NULL;
}


/**
 * Updates book from linked list
 * @param book          Link to the book to update
 * @param updated_book  Link to the book with new parameters
 */
void update_book(struct Book * book, struct Book * updated_book) {
    book->title = updated_book->title;
    book->author = updated_book->author;
    book->year = updated_book->year;
    book->book_rate = updated_book->book_rate;
}


/**
 * Updates customer from linked list
 * @param customer          Link to the customer to update
 * @param updated_customer  Link to the customer with new parameters
 */
void update_customer(Customer * customer, Customer * updated_customer) {
    customer->name = updated_customer->name;
    customer->age  = updated_customer->age;
    customer->customer_rate = updated_customer->customer_rate;
}


/**
 * Removes book from linked list
 * @param   book    Pointer to the book to remove
 */
void remove_book(struct Book * book) {
    if (book == first_book) {
        if (book->next_book != NULL) {
            first_book = book->next_book;
        } else {
            first_book = last_book = NULL;
        }
    }
    else if (book == last_book) {
        if (last_book->prev_book != NULL) {
            last_book->prev_book->next_book = NULL;
        }
        last_book = last_book->prev_book;
    }
    else {
        if (book->prev_book != NULL) book->prev_book->next_book = book->next_book;
        if (book->next_book != NULL) book->next_book->prev_book = book->prev_book;
    }
    free(book->title);
    free(book->author);
    free(book);
}


/**
 * Removes customer from linked list
 * @param   customer    Pointer to the customer to remove
 */
void remove_customer(Customer * customer) {
    if (customer == first_customer) {
        if (customer->next_customer != NULL) {
            first_customer = customer->next_customer;
        } else {
            first_customer = last_customer = NULL;
        }
    }
    else if (customer == last_customer) {
        if (last_customer->prev_customer != NULL) {
            last_customer->prev_customer->next_customer = NULL;
        }
        last_customer = last_customer->prev_customer;
    }
    else {
        if (customer->prev_customer != NULL) customer->prev_customer->next_customer = customer->next_customer;
        if (customer->next_customer != NULL) customer->next_customer->prev_customer = customer->prev_customer;
    }
    free(customer->name);
    free(customer);
}

/*
 *  Shows help in the terminal
 */
void print_help() {
    print_text("Use command:\n");
    print_text("1 - Show Books\n");
    print_text("2 - Add Book\n");
    print_text("3 - Update Book\n");
    print_text("4 - Remove Book\n");
    print_text("5 - Show Customers\n");
    print_text("6 - Add Customer\n");
    print_text("7 - Update Customer\n");
    print_text("8 - Remove Customer\n");
    print_text("9 - Exit\n");
}


/*
 * Provides a user interface to update the book
 */ 
void update_book_interface() {
    print_title("UPDATE BOOK MODE\n");
    print_question("Enter book title: ");
    char * title = read_string(TITLE_SIZE);

    struct Book * book = find_book(title);
    free(title);

    if (book == NULL) {
        print_error("There is no book with that title\n");
        return;
    }

    print_info("\nRequesting new book parameters. \n");
    struct Book * updated_book = read_book();
    update_book(book, updated_book);
    print_accept("Book updated successfully.\n");
}


/*
 * Provides a user interface to remove the book recording
 */
void remove_book_interface() {
    print_title("REMOVE BOOK MODE\n");
    print_question("Enter book title: ");
    char * title = read_string(TITLE_SIZE);
    
    struct Book * book = find_book(title);
    free(title);

    if (book == NULL) {
        print_error("There is no book with that title\n");
        return;
    }
    
    remove_book(book);

    print_accept("Book removed!\n");
}


/*
 * Provides a user interface to update customer recording
 */
void update_customer_interface() {
    print_question("Enter customer name: ");
    char * name = read_string(CUSTOMER_NAME_SIZE);

    Customer * customer = find_customer(name);
    free(name);

    if (customer == NULL) {
        print_error("There is no customer with that name\n");
        return;
    }

    print_info("\nRequesting new customer parameters: \n");
    Customer * updated_customer = read_customer();
    update_customer(customer, updated_customer);
    print_accept("Customer updated successfully.\n");
}


/*
 * Provides a user interface to remove customer recording
 */
void remove_customer_interface() {
    print_question("Enter customer name: ");
    char * name = read_string(CUSTOMER_NAME_SIZE);
    
    Customer * customer = find_customer(name);
    free(name);

    if (customer == NULL) {
        print_error("There is no customer with that name\n");
        return;
    }
    
    remove_customer(customer);

    print_accept("Customer removed!\n");
}


int main() {
    // When linked lists are empty, last_book and first_book are same
    last_book = first_book; 
    last_customer = first_customer;

    auto int command;    // User command


    while (1) {
        clear_terminal();
        print_help();
        print_question("Enter a command: ");
        scanf("%d", &command);
        tidy(stdin);
        clear_terminal();


        switch(command) {
            case 1: // Print all books
                print_title("SHOW BOOKS MODE\n");
                print_book_list();
                break;
            case 2: // Add the book
                print_title("ADD BOOK MODE\n");
                add_book(read_book());
                print_accept("Book added successfully.\n");
                break;
            case 3: // Update the book
                print_title("UPDATE CUSTOMER MODE\n");
                update_book_interface();
                break;
            case 4: // Remove the book
                print_title("REMOVE CUSTOMER MODE\n");
                remove_book_interface();
                break;
            case 5: // Show all customers
                print_title("SHOW CUSTOMERS MODE\n");
                print_customer_list();
                break;
            case 6: // Add customer
                add_customer(read_customer());
                print_accept("Customer added successfully.\n");
                break;
            case 7: // Update customer
                update_customer_interface();
                break;
            case 8: // Remove customer
                remove_customer_interface();
                break;
            case 9: // Exit the program
                print_info("Exiting...\n");
                goto exit;
            default:
                print_error("Invalid command!\n");
                break;
        }
        press_enter_to_continue();
    }
    exit:
    print_info("Thank you for choosing Surnachev Inc. SoftWare <3\n");
    return 0;
}
